import Combine

protocol LocationDataSourceProtocol {

    var authorizationStatus: AnyPublisher<AuthorizationStatusDataModel, Never> { get }

    func requestAuthorization()

    func startUpdatingLocation()

    func stopUpdatingLocation()

}

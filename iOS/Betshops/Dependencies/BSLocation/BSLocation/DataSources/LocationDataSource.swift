import Combine

class LocationDataSource: LocationDataSourceProtocol {

    private let locationService: LocationServiceProtocol

    var authorizationStatus: AnyPublisher<AuthorizationStatusDataModel, Never> {
        locationService
            .authorizationStatus
            .map { AuthorizationStatusDataModel(from: $0) }
            .subscribeOnBackground()
    }

    init(locationService: LocationServiceProtocol) {
        self.locationService = locationService
    }

    func requestAuthorization() {
        locationService
            .requestAuthorization()
    }

    func startUpdatingLocation() {
        locationService
            .startUpdatingLocation()
    }

    func stopUpdatingLocation() {
        locationService
            .stopUpdatingLocation()
    }

}

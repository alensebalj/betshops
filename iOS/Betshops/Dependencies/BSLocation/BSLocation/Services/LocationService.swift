import Combine
import CoreLocation

class LocationService: NSObject, LocationServiceProtocol {

    private let authStatusSubject = PassthroughSubject<CLAuthorizationStatus, Never>()

    private var locationManager: CLLocationManager!

    var authorizationStatus: AnyPublisher<AuthorizationStatusModel, Never> {
        authStatusSubject
            .map { AuthorizationStatusModel(from: $0) }
            .eraseToAnyPublisher()
    }

    override init() {
        super.init()

        locationManager = CLLocationManager()
        locationManager.delegate = self
    }

    func requestAuthorization() {
        locationManager.requestWhenInUseAuthorization()
        locationManager.requestAlwaysAuthorization()
    }

    func startUpdatingLocation() {
        locationManager.startUpdatingLocation()
    }

    func stopUpdatingLocation() {
        locationManager.stopUpdatingLocation()
    }

}

extension LocationService: CLLocationManagerDelegate {

    @available(iOS 14.0, *)
    func locationManagerDidChangeAuthorization(_ manager: CLLocationManager) {
        authStatusSubject.send(manager.authorizationStatus)
    }

    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        authStatusSubject.send(status)
    }

}

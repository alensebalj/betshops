import CoreLocation

enum AuthorizationStatusModel {

    case always
    case whenInUse
    case denied
    case notDetermined
    case restricted
    case unknown

}

extension AuthorizationStatusModel {

    init(from status: CLAuthorizationStatus) {
        switch status {
        case .authorizedAlways:
            self = .always
        case .authorizedWhenInUse:
            self = .whenInUse
        case .denied:
            self = .denied
        case .notDetermined:
            self = .notDetermined
        case .restricted:
            self = .restricted
        @unknown default:
            self = .unknown
        }
    }

}

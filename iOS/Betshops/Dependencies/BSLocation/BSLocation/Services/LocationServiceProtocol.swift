import Combine

protocol LocationServiceProtocol {

    var authorizationStatus: AnyPublisher<AuthorizationStatusModel, Never> { get }

    func requestAuthorization()

    func startUpdatingLocation()

    func stopUpdatingLocation()

}

import Combine

class LocationUseCase: LocationUseCaseProtocol {

    private let locationRepository: LocationRepositoryProtocol

    var authorizationStatus: AnyPublisher<AuthorizationStatusUseCaseModel, Never> {
        locationRepository
            .authorizationStatus
            .map { AuthorizationStatusUseCaseModel(from: $0) }
            .eraseToAnyPublisher()
    }

    init(locationRepository: LocationRepositoryProtocol) {
        self.locationRepository = locationRepository
    }

    func requestAuthorization() {
        locationRepository
            .requestAuthorization()
    }

    func startUpdatingLocation() {
        locationRepository
            .startUpdatingLocation()
    }

    func stopUpdatingLocation() {
        locationRepository
            .stopUpdatingLocation()
    }

}

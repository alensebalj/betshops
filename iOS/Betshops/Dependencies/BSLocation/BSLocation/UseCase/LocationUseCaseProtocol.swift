import Combine

public protocol LocationUseCaseProtocol {

    var authorizationStatus: AnyPublisher<AuthorizationStatusUseCaseModel, Never> { get }

    func requestAuthorization()

    func startUpdatingLocation()

    func stopUpdatingLocation()

}

import BSCore
import Resolver

public class LocationModule: AppModuleProtocol {

    public init() {}

    public func registerDependencies(in container: Resolver) {
        registerServices(in: container)
        registerDataSources(in: container)
        registerRepository(in: container)
        registerUseCases(in: container)
    }

    private func registerServices(in container: Resolver) {
        container
            .register { LocationService() }
            .implements(LocationServiceProtocol.self)
            .scope(.application)
    }

    private func registerDataSources(in container: Resolver) {
        container
            .register {
                LocationDataSource(locationService: container.resolve())
            }
            .implements(LocationDataSourceProtocol.self)
            .scope(.application)
    }

    private func registerRepository(in container: Resolver) {
        container
            .register {
                LocationRepository(locationDataSource: container.resolve())
            }
            .implements(LocationRepositoryProtocol.self)
            .scope(.application)
    }

    private func registerUseCases(in container: Resolver) {
        container
            .register { LocationUseCase(locationRepository: container.resolve()) }
            .implements(LocationUseCaseProtocol.self)
            .scope(.application)
    }

}

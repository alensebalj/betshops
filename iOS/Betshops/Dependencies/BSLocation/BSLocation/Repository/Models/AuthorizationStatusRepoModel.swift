public enum AuthorizationStatusRepoModel {

    case always
    case whenInUse
    case denied
    case notDetermined
    case restricted
    case unknown

}

extension AuthorizationStatusRepoModel {

    init(from model: AuthorizationStatusDataModel) {
        switch model {
        case .always:
            self = .always
        case .whenInUse:
            self = .whenInUse
        case .denied:
            self = .denied
        case .notDetermined:
            self = .notDetermined
        case .restricted:
            self = .restricted
        case .unknown:
            self = .unknown
        }
    }

}

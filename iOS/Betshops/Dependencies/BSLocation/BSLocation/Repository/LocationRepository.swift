import Combine

class LocationRepository: LocationRepositoryProtocol {

    private let locationDataSource: LocationDataSourceProtocol

    var authorizationStatus: AnyPublisher<AuthorizationStatusRepoModel, Never> {
        locationDataSource
            .authorizationStatus
            .map { AuthorizationStatusRepoModel(from: $0) }
            .eraseToAnyPublisher()
    }

    init(locationDataSource: LocationDataSourceProtocol) {
        self.locationDataSource = locationDataSource
    }

    func requestAuthorization() {
        locationDataSource
            .requestAuthorization()
    }

    func startUpdatingLocation() {
        locationDataSource
            .startUpdatingLocation()
    }

    func stopUpdatingLocation() {
        locationDataSource
            .stopUpdatingLocation()
    }

}

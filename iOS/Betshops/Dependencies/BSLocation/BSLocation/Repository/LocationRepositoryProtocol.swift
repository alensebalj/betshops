import Combine

protocol LocationRepositoryProtocol {

    var authorizationStatus: AnyPublisher<AuthorizationStatusRepoModel, Never> { get }

    func requestAuthorization()

    func startUpdatingLocation()

    func stopUpdatingLocation()

}

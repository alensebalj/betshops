import BSCore
import Combine
import UIKit

// MARK: Gestures
public extension UIView {

    func gesture(_ gestureType: GestureType) -> GesturePublisher {
        GesturePublisher(view: self, gestureType: gestureType)
    }

    var tap: GesturePublisher {
        gesture(.tap())
    }

    func throttledTap(for interval: Double = 0.5) -> AnyPublisher<GesturePublisher.Output, Never> {
        tap
            .throttle(for: .seconds(interval), scheduler: DispatchQueue.main, latest: true)
            .eraseToAnyPublisher()
    }

}

// MARK: Corners
public extension UIView {

    func roundTopCorners(withRadius radius: CGFloat) {
        layer.cornerRadius = radius
        layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
    }

    func roundBottomCorners(withRadius radius: CGFloat) {
        layer.cornerRadius = radius
        layer.maskedCorners = [.layerMaxXMaxYCorner, .layerMinXMaxYCorner]
    }

    func roundAllCorners(withRadius radius: CGFloat) {
        layer.cornerRadius = radius
        layer.maskedCorners = [
            .layerMinXMinYCorner,
            .layerMaxXMinYCorner,
            .layerMaxXMaxYCorner,
            .layerMinXMaxYCorner]
    }

}

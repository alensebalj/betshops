import UIKit

public extension CGFloat {

    static let defaultGutter: CGFloat = 11
    static let alphaEnabled: CGFloat = 1
    static let alphaDisabled: CGFloat = 0.2

    static func gutter(
        baseValue: CGFloat = defaultGutter,
        withMultiplier multiplier: CGFloat = 1.0,
        withOffset offset: CGFloat = 0
    ) -> CGFloat {
        return (baseValue * multiplier) + offset
    }

}

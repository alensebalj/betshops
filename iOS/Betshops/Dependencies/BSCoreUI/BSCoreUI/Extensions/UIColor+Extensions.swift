import UIKit

public extension UIColor {

    static var neutral1000: UIColor {
        UIColor(rgb: 0x4A4F54)
    }

    static var neutral1000a55: UIColor {
        UIColor(rgb: 0x4A4F54).withAlphaComponent(0.55)
    }

    // MARK: - Init
    private convenience init(red: Int, green: Int, blue: Int) {
        assert(red >= 0 && red <= 255, "Invalid red component")
        assert(green >= 0 && green <= 255, "Invalid green component")
        assert(blue >= 0 && blue <= 255, "Invalid blue component")

        self.init(red: CGFloat(red) / 255.0, green: CGFloat(green) / 255.0, blue: CGFloat(blue) / 255.0, alpha: 1.0)
    }

    convenience init(rgb: Int) {
        self.init(
            red: (rgb >> 16) & 0xFF,
            green: (rgb >> 8) & 0xFF,
            blue: rgb & 0xFF)
    }

}

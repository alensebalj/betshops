import UIKit

public class PassthroughViewController: UIViewController {

    public override func viewDidLoad() {
        super.viewDidLoad()

        view = TouchDelegatingView()

        if let delegatingView = view as? TouchDelegatingView {
            delegatingView.touchDelegate = presentingViewController?.view
        }
    }

}

class TouchDelegatingView: UIView {

    weak var touchDelegate: UIView?

    override func hitTest(_ point: CGPoint, with event: UIEvent?) -> UIView? {
        guard let view = super.hitTest(point, with: event) else { return nil }

        guard
            view === self,
            let point = touchDelegate?.convert(point, from: self)
        else {
            return view
        }

        return touchDelegate?.hitTest(point, with: event)
    }

}

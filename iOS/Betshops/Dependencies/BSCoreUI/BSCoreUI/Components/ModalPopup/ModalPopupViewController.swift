import BSCore
import Combine
import UIKit
import SnapKit

public class ModalPopupViewController: PassthroughViewController {

    let headerHeight: CGFloat = 30

    public var childViewController: UIViewController!

    var headerView: UIView!
    var knobView: UIView!
    var containerView: UIView!
    var containerViewConstraint: Constraint!

    private var disposables = Set<AnyCancellable>()

    var availableHeight: CGFloat {
        view.frame.height - UIEdgeInsets.safeAreaInsets.top
    }

    var containerViewHeight: CGFloat {
        childViewController.view.frame.height
    }

    var modalViewHeight: CGFloat {
        let calculatedHeight = containerViewHeight + containerTopContentHeight
        return min(calculatedHeight, availableHeight)
    }

    var containerTopContentHeight: CGFloat {
        UIEdgeInsets.safeAreaInsets.bottom + headerHeight
    }

    var closedPositionOffset: CGFloat {
        modalViewHeight + UIEdgeInsets.safeAreaInsets.top
    }

    var openedPositionOffset: CGFloat {
        0
    }

    public convenience init(childViewController: UIViewController) {
        self.init(nibName: nil, bundle: nil)

        self.childViewController = childViewController
    }

    public override func viewDidLoad() {
        super.viewDidLoad()

        createViews()
        styleViews()
        defineLayoutForViews()
        bindViews()
    }

    public override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)

        animateOpen()
    }

    public func close() {
        animateClose()
    }

    private func bindViews() {
       headerView
            .gesture(.pan())
            .compactMap { $0.recognizer as? UIPanGestureRecognizer }
            .sink(receiveValue: { [weak self] recognizer in
                self?.handlePan(recognizer: recognizer)
            })
            .store(in: &disposables)
    }

    public override func preferredContentSizeDidChange(forChildContentContainer container: UIContentContainer) {
        super.preferredContentSizeDidChange(forChildContentContainer: container)

        applyHeightConstraintIfNeeded(container.preferredContentSize.height)
    }

    func applyHeightConstraintIfNeeded(_ preferredContentHeight: CGFloat) {
        let preferredJointHeight = containerTopContentHeight + preferredContentHeight
        let maxHeight = availableHeight * 0.9
        let preferredHeight = preferredJointHeight > availableHeight ? maxHeight : preferredJointHeight

        containerView.snp.makeConstraints {
            $0.height.equalTo(preferredHeight)
        }
        view.layoutIfNeeded()
    }

    private func handlePan(recognizer: UIPanGestureRecognizer) {
        guard
            let view = recognizer.view,
            recognizer.state != .began
        else {
            return
        }

        if recognizer.state == .cancelled {
            animateOpen()
            return
        }

        let translation = recognizer.translation(in: view)

        if recognizer.state == .ended {
            let hasStartedToClose = translation.y > 0
            if hasStartedToClose {
                animateClose()
            }
            return
        }

        let translationY = max(translation.y, 0)

        let newOffset = openedPositionOffset - translationY
        containerViewConstraint.update(offset: -newOffset)
        self.view.layoutIfNeeded()
    }

}

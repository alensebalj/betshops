import BSCore
import UIKit

extension ModalPopupViewController {

    func animateOpen() {
        UIView.animate(withDuration: AnimationConstants.short) {
            self.containerViewConstraint.update(offset: self.openedPositionOffset)
            self.view.layoutIfNeeded()
        }
    }

    func animateClose(completion: (() -> Void)? = nil) {
        UIView.animate(
            withDuration: AnimationConstants.short,
            animations: {
                self.containerViewConstraint.update(offset: self.closedPositionOffset)
                self.view.layoutIfNeeded()
            },
            completion: { [weak self] _ in
                self?.dismiss(animated: false, completion: completion)
            })
    }

}

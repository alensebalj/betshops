import BSCore
import SnapKit
import UIKit

extension ModalPopupViewController: ConstructViewsProtocol {

    public func createViews() {
        containerView = UIView()
        view.addSubview(containerView)

        headerView = UIView()
        containerView.addSubview(headerView)

        knobView = UIView()
        headerView.addSubview(knobView)

        addChild(childViewController)
        containerView.addSubview(childViewController.view)
    }

    public func styleViews() {
        containerView.backgroundColor = .white
        containerView.roundTopCorners(withRadius: 10)

        knobView.backgroundColor = .neutral1000a55
        knobView.roundAllCorners(withRadius: 2)
    }

    public func defineLayoutForViews() {
        containerView.snp.makeConstraints {
            $0.leading.trailing.equalToSuperview()
            containerViewConstraint = $0.bottom.equalTo(view.snp.bottom).offset(closedPositionOffset).constraint
        }

        headerView.snp.makeConstraints {
            $0.leading.top.trailing.equalToSuperview()
            $0.height.equalTo(headerHeight)
        }

        knobView.snp.makeConstraints {
            $0.size.equalTo(CGSize(width: 30, height: 4))
            $0.center.equalToSuperview()
        }

        childViewController.view.snp.makeConstraints {
            $0.top.equalTo(headerView.snp.bottom)
            $0.leading.bottom.trailing.equalToSuperview()
        }
    }

}

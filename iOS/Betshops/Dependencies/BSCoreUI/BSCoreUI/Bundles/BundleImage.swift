import Foundation

public enum BundleImage: String {

    case close = "ic_close"
    case phone = "ic_phone"
    case pin = "ic_pin"
    case shopSelected = "ic_shop_selected"
    case shop = "ic_shop"

}

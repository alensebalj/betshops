import Combine

public typealias Completable<Failure: Error> = AnyPublisher<Never, Failure>

public extension Completable {

    static func from(_ action: @escaping () -> Void) -> Completable<Never> {
        Deferred<Empty<Never, Never>> {
            action()
            return Empty<Never, Never>()
        }.eraseToAnyPublisher()
    }

}

public extension Completable {

    func run(in store: inout Set<AnyCancellable>) {
        sink(receiveCompletion: { _ in }, receiveValue: { _ in })
            .store(in: &store)
    }

}

public extension Completable where Output == Never {

    func sink(receiveCompletion: @escaping (Subscribers.Completion<Failure>) -> Void) -> AnyCancellable {
        sink(receiveCompletion: receiveCompletion, receiveValue: { _ in })
    }

}

public extension Completable where Failure == Never {

    static func empty() -> Completable<Never> {
        from { }
    }

    static func never() -> Completable<Never> {
        Deferred<Future<Never, Never>> {
            Future<Never, Never> { _ in }
        }.eraseToAnyPublisher()
    }

}

public extension Publisher {

    func asCompletable() -> Completable<Failure> {
        ignoreOutput()
            .eraseToAnyPublisher()
    }

}

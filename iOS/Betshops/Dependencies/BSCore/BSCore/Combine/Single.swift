import Combine

public typealias Single<Output, Failure: Error> = AnyPublisher<Output, Failure>

public extension Single {

    static func from(_ outerPromise: @escaping ((Result<Output, Failure>) -> Void) -> Void) -> Single<Output, Failure> {
        Deferred<Future<Output, Failure>> {
            Future<Output, Failure> { promise in
                outerPromise(promise)
            }
        }.eraseToAnyPublisher()
    }

    static func error(error: Failure) -> Single<Output, Failure> {
        Deferred<Fail<Output, Failure>> {
            Fail<Output, Failure>(error: error)
        }.eraseToAnyPublisher()
    }

    static func just(output: Output) -> Single<Output, Failure> {
        Deferred<AnyPublisher<Output, Failure>> {
            Just(output).setFailureType(to: Failure.self).eraseToAnyPublisher()
        }.eraseToAnyPublisher()
    }

    static func never() -> Single<Output, Failure> {
        Deferred<Empty<Output, Failure>> {
            Empty<Output, Failure>(completeImmediately: false)
        }.eraseToAnyPublisher()
    }

}

public extension Publisher {

    func asSingle() -> Single<Output, Failure> {
        prefix(1)
            .eraseToAnyPublisher()
    }

}

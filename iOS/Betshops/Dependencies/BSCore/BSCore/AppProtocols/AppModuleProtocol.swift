import Resolver

public protocol AppModuleProtocol {

    /**
     When implementing this method register all objects provided by this module in global container
     */
    func registerDependencies(in: Resolver)

}

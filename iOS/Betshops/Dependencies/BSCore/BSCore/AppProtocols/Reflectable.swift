public protocol Reflectable: Encodable {

    var properties: [String: String] { get }

}

public extension Reflectable {

    var properties: [String: String] {
        var pairs: [String: String] = [:]
        let mirror = Mirror(reflecting: self)
        for case let (label?, value) in mirror.children {
            pairs[label] = "\(value)"
        }
        return pairs
    }

}

extension Date {

    public func isbetween(startDate: Date, endDate: Date) -> Bool {
        (min(startDate, endDate)...max(startDate, endDate)).contains(self)
    }

    public func toString(dateFormat: DateFormat) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = dateFormat.rawValue
        return dateFormatter.string(from: self)
    }

}

public enum DateFormat: String {

    case hours = "HH:mm"

}

extension String {

    public func trim() -> String {
        trimmingCharacters(in: .whitespacesAndNewlines)
    }

}

// MARK: Date
extension String {

    public func currentDateFromHours(dateFormat: DateFormat) -> Date? {
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale(identifier: "en_US_POSIX")
        dateFormatter.dateFormat = dateFormat.rawValue

        guard let date = dateFormatter.date(from: self) else { return nil }

        let calendar = Calendar.current
        let hour = calendar.component(.hour, from: date)
        return calendar.date(bySettingHour: hour, minute: 0, second: 0, of: Date())
    }

}

/**
 Bundle extension that provides an access to module's bundle.

 Implement this in every module where you have to access that module's bundle.
 That's necessary because calling `Bundle(for: AnyClass)` with `AnyClass` defined inside the module **A** returns
 a bundle for that module **A** (local bundle). That's why there's a `BundleToken` class which is defined inside
 a module and allows Bundle init to resolve the right bundle for that module.

 Extension was created so that accessing local bundle remains the same across all modules but still resolves local
 bundle for that module.

 Module A
    ```
        // Resolves bundle for the module A
        let bundle = Bundle.local
    ```

 Module B
    ```
        // Resolves bundle for the module B
        let bundle = Bundle.local
    ```
 */
extension Bundle {

    private final class BundleToken {}

    /**
     Provides an access to the module's bundle.
     */
    static var local: Bundle {
        Bundle(for: BundleToken.self)
    }

}

import BSCore
import BSCoreUI
import BSNetwork
import Resolver

public class SearchShopsModule: AppModuleProtocol {

    public init() {}

    public func registerDependencies(in container: Resolver) {
        registerClients(in: container)
        registerDataSources(in: container)
        registerRepositories(in: container)
        registerUseCases(in: container)
        registerPresenters(in: container)
        registerViewControllers(in: container)
    }

    private func registerClients(in container: Resolver) {
        container
            .register {
                SearchShopsClient(baseClient: container.resolve(name: NetworkModuleDependencies.apiBaseUrl.name))
            }
            .implements(SearchShopsClientProtocol.self)
            .scope(.application)
    }

    private func registerDataSources(in container: Resolver) {
        container
            .register { SearchShopsDataSource(searchShopsClient: container.resolve()) }
            .implements(SearchShopsDataSourceProtocol.self)
            .scope(.application)
    }

    private func registerRepositories(in container: Resolver) {
        container
            .register { SearchShopsRepository(dataSource: container.resolve()) }
            .implements(SearchShopsRepositoryProtocol.self)
            .scope(.application)
    }

    private func registerUseCases(in container: Resolver) {
        container
            .register { SearchShopsUseCase(searchShopsRepository: container.resolve()) }
            .implements(SearchShopsUseCaseProtocol.self)
            .scope(.application)
    }

    private func registerPresenters(in container: Resolver) {
        container
            .register {
                SearchShopsPresenter(
                    locationUseCase: container.resolve(),
                    searchShopsUseCase: container.resolve(),
                    router: container.resolve())
            }
            .scope(.unique)
    }

    private func registerViewControllers(in container: Resolver) {
        container
            .register { SearchShopsViewController(presenter: container.resolve()) }
            .scope(.unique)

        container
            .register { (_, args) -> ShopDetailsViewController? in
                ShopDetailsViewController(viewModel: args.get())
            }
            .scope(.unique)

        container
            .register { (_, args) -> ModalPopupViewController? in
                let viewController: ShopDetailsViewController = container.resolve(args: args.get())
                return ModalPopupViewController(childViewController: viewController)
            }
            .scope(.unique)
    }

}

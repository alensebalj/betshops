import BSCore

class SearchShopsUseCase: SearchShopsUseCaseProtocol {

    private let searchShopsRepository: SearchShopsRepositoryProtocol

    init(searchShopsRepository: SearchShopsRepositoryProtocol) {
        self.searchShopsRepository = searchShopsRepository
    }

    func queryShops(boundingBox: String) -> Single<[ShopModel], Error> {
        searchShopsRepository
            .queryShops(boundingBox: boundingBox)
            .compactMap { [weak self] in
                self?.mapToModels($0)
            }
            .asSingle()
    }

    private func mapToModels(_ repoModels: [ShopRepoModel]) -> [ShopModel] {
        repoModels
            .map { ShopModel(from: $0) }
    }

}

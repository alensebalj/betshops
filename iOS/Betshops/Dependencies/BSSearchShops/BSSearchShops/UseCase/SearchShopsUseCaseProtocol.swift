import BSCore

protocol SearchShopsUseCaseProtocol {

    func queryShops(boundingBox: String) -> Single<[ShopModel], Error>

}

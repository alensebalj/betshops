struct LocationModel {

    let latitude: Double
    let longitude: Double

}

extension LocationModel {

    init(from model: LocationRepoModel) {
        latitude = model.latitude
        longitude = model.longitude
    }

}

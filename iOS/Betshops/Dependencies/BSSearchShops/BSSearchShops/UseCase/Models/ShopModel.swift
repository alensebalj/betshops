struct ShopModel {

    let id: Int
    let name: String
    let location: LocationModel
    let county: String
    let cityId: Int
    let city: String
    let address: String

}

extension ShopModel {

    init(from model: ShopRepoModel) {
        id = model.id
        name = model.name
        location = LocationModel(from: model.location)
        county = model.county
        cityId = model.cityId
        city = model.city
        address = model.address
    }

}

import BSCore
import BSNetwork

class SearchShopsClient: SearchShopsClientProtocol {

    private let baseClient: ApiClientProtocol

    init(baseClient: ApiClientProtocol) {
        self.baseClient = baseClient
    }

    func queryShops(request: ShopRequest) -> Single<ShopResponseWrapper, Error> {
        baseClient
            .get(path: "/betshops", queryParameters: request.properties)
    }

}

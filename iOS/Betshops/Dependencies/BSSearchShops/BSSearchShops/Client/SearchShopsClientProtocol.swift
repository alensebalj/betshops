import BSCore

protocol SearchShopsClientProtocol {

    func queryShops(request: ShopRequest) -> Single<ShopResponseWrapper, Error>

}

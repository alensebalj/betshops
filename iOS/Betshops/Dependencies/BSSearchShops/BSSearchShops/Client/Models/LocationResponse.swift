struct LocationResponse: Decodable {

    let lat: Double
    let lng: Double

}

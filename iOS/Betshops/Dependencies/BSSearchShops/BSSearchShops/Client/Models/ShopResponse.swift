struct ShopResponse: Decodable {

    let id: Int
    let name: String
    let location: LocationResponse
    let county: String
    let cityId: Int
    let city: String
    let address: String

    enum CodingKeys: String, CodingKey {

        case id
        case name
        case location
        case county
        case cityId = "city_id"
        case city
        case address

    }

}

struct ShopResponseWrapper: Decodable {

    let count: Int
    let betshops: [ShopResponse]

}

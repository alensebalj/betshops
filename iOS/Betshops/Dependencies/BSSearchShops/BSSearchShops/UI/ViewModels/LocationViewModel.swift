import MapKit

struct LocationViewModel {

    let latitude: Double
    let longitude: Double
    let span: LocationSpanViewModel?

}

extension LocationViewModel {

    struct LocationSpanViewModel {

        let latitudeDelta: Double
        let longitudeDelta: Double

        var span: MKCoordinateSpan {
            MKCoordinateSpan(latitudeDelta: latitudeDelta, longitudeDelta: longitudeDelta)
        }

    }

    var region: MKCoordinateRegion? {
        guard let span = span else { return nil }

        let coordinate = CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
        return MKCoordinateRegion(center: coordinate, span: span.span)
    }

}

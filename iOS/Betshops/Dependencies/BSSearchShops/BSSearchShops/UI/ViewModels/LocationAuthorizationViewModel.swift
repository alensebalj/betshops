import BSLocation

enum LocationAuthorizationViewModel {

    case always
    case whenInUse
    case denied
    case notDetermined
    case restricted
    case unknown

}

extension LocationAuthorizationViewModel {

    init(from model: AuthorizationStatusUseCaseModel) {
        switch model {
        case .always:
            self = .always
        case .whenInUse:
            self = .whenInUse
        case .denied:
            self = .denied
        case .notDetermined:
            self = .notDetermined
        case .restricted:
            self = .restricted
        case .unknown:
            self = .unknown
        }
    }

}

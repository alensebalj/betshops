public struct ShopViewModel {

    let id: String
    let name: String
    let city: CityViewModel
    let location: LocationViewModel
    let openTime: Date?
    let closeTime: Date?

}

extension ShopViewModel {

    struct CityViewModel {

        let id: String
        let cityName: String
        let address: String
        let county: String

    }

    init(from model: ShopModel) {
        id = "\(model.id)"
        name = model.name.trim()

        city = CityViewModel(
            id: "\(model.cityId)",
            cityName: model.city.trim(),
            address: model.address.trim(),
            county: model.county.trim())

        location = LocationViewModel(
            latitude: model.location.latitude,
            longitude: model.location.longitude,
            span: nil)

        openTime = "08:00".currentDateFromHours(dateFormat: .hours)
        closeTime = "16:00".currentDateFromHours(dateFormat: .hours)
    }

}

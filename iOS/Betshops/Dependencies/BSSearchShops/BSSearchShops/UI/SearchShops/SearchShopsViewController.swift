import BSCore
import BSCoreUI
import Combine
import MapKit
import UIKit

public class SearchShopsViewController: UIViewController {

    var mapView: ShopsMapView!

    private var disposables = Set<AnyCancellable>()
    private var presenter: SearchShopsPresenter!

    convenience init(presenter: SearchShopsPresenter) {
        self.init()

        self.presenter = presenter
    }

    public override func viewDidLoad() {
        super.viewDidLoad()

        buildViews()
        bindViews()
        bindPresenter()
    }

    public override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)

        presenter.requestLocationAuthorization()
    }

    public override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)

        presenter.stopUpdatingLocation()
    }

    private func bindViews() {
        presenter
            .locationAuthorizationStatus
            .sink { [weak self] status in
                guard let self = self else { return }

                switch status {
                case .always, .whenInUse:
                    self.mapView.showsUserLocation = true
                default:
                    self.setDefaultLocation()
                }
            }
            .store(in: &disposables)

        mapView
            .annotationSelected
            .compactMap { ($0 as? IdentifiedPointAnnotation)?.id }
            .sink { [weak self] viewModelId in
                self?.handleAnnotationSelected(with: viewModelId)
            }
            .store(in: &disposables)
    }

    func bindPresenter() {
        presenter
            .shopViewModels
            .sink { [weak self] viewModels in
                self?.createAnnotations(for: viewModels)
            }
            .store(in: &disposables)
    }

    private func setDefaultLocation() {
        guard let region = presenter.defaultLocation.region else { return }

        setMapCenterTo(region: region)
    }

    private func setMapCenterTo(region: MKCoordinateRegion) {
        mapView.setRegion(region, animated: true)
    }

    private func handleAnnotationSelected(with id: String) {
        presentedViewController == nil ? presenter.showDetails(viewModelId: id) : updateDetails(viewModelId: id)
    }

    private func updateDetails(viewModelId: String) {
        guard
            let presentedViewController = presentedViewController as? ModalPopupViewController,
            let shopDetailsViewController = presentedViewController.childViewController as? ShopDetailsViewController
        else {
            return
        }

        presenter.updateDetails(shopDetailsViewController, viewModelId: viewModelId)
    }

    private func createAnnotations(for viewModels: [ShopViewModel]) {
        let annotations = viewModels.map { annotation(for: $0) }
        mapView.addAndShowAnnotations(annotations)
    }

    private func annotation(for viewModel: ShopViewModel) -> MKAnnotation {
        let annotation = IdentifiedPointAnnotation()
        annotation.title = viewModel.name
        annotation.coordinate = CLLocationCoordinate2D(
            latitude: viewModel.location.latitude,
            longitude: viewModel.location.longitude)
        annotation.id = String(viewModel.id)
        return annotation
    }

}

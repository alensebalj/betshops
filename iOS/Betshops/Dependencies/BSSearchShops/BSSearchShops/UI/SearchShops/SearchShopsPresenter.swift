import BSCore
import BSLocation
import Combine

class SearchShopsPresenter {

    private let zoomDelta: Double = 0.2
    private let defaultLocationCoordinate = (latitude: 48.137154, longitude: 11.576124)
    private let defaultBoundingBox = "48.16124,11.60912,48.12229,11.52741"

    private let locationUseCase: LocationUseCaseProtocol
    private let searchShopsUseCase: SearchShopsUseCaseProtocol
    private let router: SearchShopsRouterProtocol

    private let shopViewModelSubject = CurrentValueSubject<[ShopViewModel], Never>([])
    private var disposables = Set<AnyCancellable>()

    var shopViewModels: AnyPublisher<[ShopViewModel], Never> {
        shopViewModelSubject
            .eraseToAnyPublisher()
    }

    var locationAuthorizationStatus: AnyPublisher<LocationAuthorizationViewModel, Never> {
        locationUseCase
            .authorizationStatus
            .map { LocationAuthorizationViewModel(from: $0) }
            .receiveOnMain()
    }

    var defaultLocation: LocationViewModel {
        let span = LocationViewModel.LocationSpanViewModel(latitudeDelta: zoomDelta, longitudeDelta: zoomDelta)

        return LocationViewModel(
            latitude: defaultLocationCoordinate.latitude,
            longitude: defaultLocationCoordinate.longitude,
            span: span)
    }

    init(
        locationUseCase: LocationUseCaseProtocol,
        searchShopsUseCase: SearchShopsUseCaseProtocol,
        router: SearchShopsRouterProtocol
    ) {
        self.locationUseCase = locationUseCase
        self.searchShopsUseCase = searchShopsUseCase
        self.router = router

        getDefaultShops()
    }

    func requestLocationAuthorization() {
        locationUseCase
            .requestAuthorization()
    }

    func startUpdatingLocation() {
        locationUseCase
            .startUpdatingLocation()
    }

    func stopUpdatingLocation() {
        locationUseCase
            .stopUpdatingLocation()
    }

    func showDetails(viewModelId: String) {
        let shops = shopViewModelSubject.value.map { ($0.id, $0) }
        let viewModelsDict = Dictionary(shops, uniquingKeysWith: { a, _ in a })

        if let viewModel = viewModelsDict[viewModelId] {
            router.showDetails(with: viewModel)
        }
    }

    func updateDetails(_ viewController: ShopDetailsViewController, viewModelId: String) {
        let shops = shopViewModelSubject.value.map { ($0.id, $0) }
        let viewModelsDict = Dictionary(shops, uniquingKeysWith: { a, _ in a })

        if let viewModel = viewModelsDict[viewModelId] {
            viewController.update(with: viewModel)
        }
    }

    private func getDefaultShops() {
        searchShopsUseCase
            .queryShops(boundingBox: defaultBoundingBox)
            .receiveOnMain()
            .compactMap { [weak self] in
                self?.mapToViewModels($0)
            }
            .sink(
                receiveCompletion: { _ in },
                receiveValue: { [weak self] viewModels in
                    self?.shopViewModelSubject.send(viewModels)
                })
            .store(in: &disposables)
    }

    private func mapToViewModels(_ models: [ShopModel]) -> [ShopViewModel] {
        models
            .map { ShopViewModel(from: $0) }
    }

}

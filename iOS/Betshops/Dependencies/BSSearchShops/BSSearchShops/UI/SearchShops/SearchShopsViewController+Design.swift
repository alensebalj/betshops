import BSCore
import MapKit
import SnapKit

extension SearchShopsViewController: ConstructViewsProtocol {

    func buildViews() {
        createViews()
        styleViews()
        defineLayoutForViews()
    }

    public func createViews() {
        mapView = ShopsMapView()
        view.addSubview(mapView)
    }

    public func styleViews() {
        title = LocalizableStrings.betShops.localized

        view.backgroundColor = .white

        mapView.showsUserLocation = true
    }

    public func defineLayoutForViews() {
        mapView.snp.makeConstraints { $0.edges.equalToSuperview() }
    }

}

import BSCore
import BSCoreUI
import UIKit

extension ShopAnnotationView: ConstructViewsProtocol {

    func buildViews() {
        createViews()
        styleViews()
    }

    func createViews() {
    }

    func styleViews() {
        setSelected(false)
        canShowCallout = true
    }

    func defineLayoutForViews() {
    }

}

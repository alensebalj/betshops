import MapKit

class ShopAnnotationView: MKAnnotationView {

    let imageSize = CGSize(width: 35, height: 50)
    let imageSizeSelected = CGSize(width: 50, height: 65)

    static let reuseIdentifier = String(describing: ShopAnnotationView.self)

    override init(annotation: MKAnnotation?, reuseIdentifier: String?) {
        super.init(annotation: annotation, reuseIdentifier: reuseIdentifier)

        buildViews()
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    func setSelected(_ isSelected: Bool) {
        let image = isSelected ? UIImage(with: .shopSelected) : UIImage(with: .shop)
        let imageSize = isSelected ? imageSizeSelected : imageSize

        UIGraphicsBeginImageContext(imageSize)
        image.draw(in: CGRect(origin: .zero, size: imageSize))
        let resizedImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()

        self.image = resizedImage ?? image
    }

}

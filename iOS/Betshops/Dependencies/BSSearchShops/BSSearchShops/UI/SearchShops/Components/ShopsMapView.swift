import Combine
import MapKit

class ShopsMapView: MKMapView {

    private let annotationSelectedSubject = PassthroughSubject<MKAnnotation, Never>()

    var annotationSelected: AnyPublisher<MKAnnotation, Never> {
        annotationSelectedSubject
            .eraseToAnyPublisher()
    }

    init() {
        super.init(frame: .zero)

        delegate = self
        register(ShopAnnotationView.self, forAnnotationViewWithReuseIdentifier: ShopAnnotationView.reuseIdentifier)
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    func addAndShowAnnotations(
        _ newAnnotations: [MKAnnotation],
        removePreviousAnnotations: Bool = false
    ) {
        if removePreviousAnnotations {
            removeAnnotations(annotations)
        }

        addAnnotations(newAnnotations)
        showAnnotations(newAnnotations, animated: true)
    }

}

extension ShopsMapView: MKMapViewDelegate {

    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        guard annotation is MKPointAnnotation else { return nil }

        if let annotationView = dequeueReusableAnnotationView(withIdentifier: ShopAnnotationView.reuseIdentifier) {
            annotationView.annotation = annotation
            return annotationView
        }

        let pinAnnotationView = MKPinAnnotationView(
            annotation: annotation,
            reuseIdentifier: ShopAnnotationView.reuseIdentifier)
        pinAnnotationView.canShowCallout = true

        return pinAnnotationView
    }

    func mapView(_ mapView: MKMapView, didSelect view: MKAnnotationView) {
        guard
            let annotationView = view as? ShopAnnotationView,
            let annotation = annotationView.annotation
        else {
            return
        }

        annotationView.setSelected(true)
        annotationSelectedSubject.send(annotation)
    }

    func mapView(_ mapView: MKMapView, didDeselect view: MKAnnotationView) {
        guard let annotationView = view as? ShopAnnotationView else { return }

        annotationView.setSelected(false)
    }

}

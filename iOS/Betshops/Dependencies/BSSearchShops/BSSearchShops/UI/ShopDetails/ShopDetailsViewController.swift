import Combine
import UIKit

public class ShopDetailsViewController: UIViewController {

    let imageSize = CGSize(width: 15, height: 20)
    let buttonHeight: CGFloat = 50
    let defaultMargin: CGFloat = 16
    let regionDistance: Double = 10000

    var pinImageView: UIImageView!
    var phoneImageView: UIImageView!
    var stackView: UIStackView!
    var nameLabel: UILabel!
    var addressLabel: UILabel!
    var cityLabel: UILabel!
    var workingTimeLabel: UILabel!
    var routeButton: UIButton!

    private var disposables = Set<AnyCancellable>()
    var viewModel: ShopViewModel!

    convenience init(viewModel: ShopViewModel) {
        self.init()

        self.viewModel = viewModel
    }

    public override func viewDidLoad() {
        super.viewDidLoad()

        buildViews()
        bindViews()
    }

    private func bindViews() {
        routeButton
            .throttledTap()
            .sink { [weak self] _ in
                self?.openInMaps()
            }
            .store(in: &disposables)
    }

    func update(with viewModel: ShopViewModel) {
        self.viewModel = viewModel

        nameLabel.text = viewModel.name
        addressLabel.text = viewModel.city.address
        cityLabel.text = LocalizableStrings.cityCounty.localized(with: [viewModel.city.cityName, viewModel.city.county])
        setWorkingTimeLabel(openTime: viewModel.openTime, closeTime: viewModel.closeTime)
    }

    func setWorkingTimeLabel(openTime: Date?, closeTime: Date?) {
        guard
            let openTime = openTime,
            let closeTime = closeTime
        else {
            workingTimeLabel.isHidden = true
            return
        }

        let text = Date().isbetween(startDate: openTime, endDate: closeTime) ?
        LocalizableStrings.openUntil.localized(with: [closeTime.toString(dateFormat: .hours)]) :
        LocalizableStrings.opensTomorrow.localized(with: [openTime.toString(dateFormat: .hours)])

        workingTimeLabel.text = text
    }

}

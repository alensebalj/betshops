import MapKit

extension ShopDetailsViewController {

    func openInMaps() {
        let coordinates = CLLocationCoordinate2DMake(viewModel.location.latitude, viewModel.location.longitude)
        let regionSpan = MKCoordinateRegion(
            center: coordinates,
            latitudinalMeters: regionDistance,
            longitudinalMeters: regionDistance)
        let options = [
            MKLaunchOptionsMapCenterKey: NSValue(mkCoordinate: regionSpan.center),
            MKLaunchOptionsMapSpanKey: NSValue(mkCoordinateSpan: regionSpan.span)
        ]
        let placemark = MKPlacemark(coordinate: coordinates)
        let mapItem = MKMapItem(placemark: placemark)
        mapItem.name = viewModel.name
        mapItem.openInMaps(launchOptions: options)
    }

}

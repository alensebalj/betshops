import BSCore
import BSCoreUI
import SnapKit
import UIKit

extension ShopDetailsViewController: ConstructViewsProtocol {

    func buildViews() {
        createViews()
        styleViews()
        defineLayoutForViews()
    }

    public func createViews() {
        pinImageView = UIImageView()
        view.addSubview(pinImageView)

        phoneImageView = UIImageView()
        view.addSubview(phoneImageView)

        stackView = UIStackView()
        view.addSubview(stackView)

        nameLabel = UILabel()
        stackView.addArrangedSubview(nameLabel)

        addressLabel = UILabel()
        stackView.addArrangedSubview(addressLabel)

        cityLabel = UILabel()
        stackView.addArrangedSubview(cityLabel)

        workingTimeLabel = UILabel()
        stackView.addArrangedSubview(workingTimeLabel)

        routeButton = UIButton()
        view.addSubview(routeButton)
    }

    public func styleViews() {
        view.backgroundColor = .white

        pinImageView.image = UIImage(with: .pin)

        phoneImageView.image = UIImage(with: .phone)

        stackView.axis = .vertical
        stackView.layoutMargins = .insets(left: 16, right: 16)
        stackView.isLayoutMarginsRelativeArrangement = true
        stackView.spacing = 2
        stackView.setCustomSpacing(defaultMargin, after: cityLabel)

        nameLabel.textColor = .neutral1000
        nameLabel.text = viewModel.name
        nameLabel.numberOfLines = 0

        addressLabel.textColor = .neutral1000
        addressLabel.text = viewModel.city.address

        cityLabel.textColor = .neutral1000
        cityLabel.text = LocalizableStrings.cityCounty.localized(with: [viewModel.city.cityName, viewModel.city.county])

        workingTimeLabel.textColor = .neutral1000
        setWorkingTimeLabel(openTime: viewModel.openTime, closeTime: viewModel.closeTime)

        routeButton.setTitle(LocalizableStrings.route.localized, for: .normal)
        routeButton.setTitleColor(.white, for: .normal)
        routeButton.backgroundColor = .blue
        routeButton.titleLabel?.textAlignment = .center
        routeButton.roundAllCorners(withRadius: buttonHeight / 2)
    }

    public func defineLayoutForViews() {
        pinImageView.snp.makeConstraints {
            $0.top.equalToSuperview()
            $0.leading.equalToSuperview().offset(defaultMargin)
            $0.size.equalTo(imageSize)
        }

        routeButton.snp.makeConstraints {
            $0.bottom.equalTo(view.safeAreaLayoutGuide).inset(defaultMargin)
            $0.leading.trailing.equalToSuperview().inset(defaultMargin)
            $0.height.equalTo(buttonHeight)
        }

        stackView.snp.makeConstraints {
            $0.top.equalTo(pinImageView)
            $0.leading.equalTo(pinImageView.snp.trailing)
            $0.trailing.equalToSuperview()
            $0.bottom.equalTo(routeButton.snp.top).offset(-defaultMargin)
        }

        phoneImageView.snp.makeConstraints {
            $0.leading.equalToSuperview().offset(defaultMargin)
            $0.size.equalTo(imageSize)
            $0.bottom.equalTo(stackView)
        }
    }

}

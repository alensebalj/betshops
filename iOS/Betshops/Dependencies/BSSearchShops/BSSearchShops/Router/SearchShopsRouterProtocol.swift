public protocol SearchShopsRouterProtocol {

    func goToSearchShopsViewController()

    func showDetails(with viewModel: ShopViewModel)

}

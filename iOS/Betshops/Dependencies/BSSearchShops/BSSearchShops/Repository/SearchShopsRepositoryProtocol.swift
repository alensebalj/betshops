import BSCore

protocol SearchShopsRepositoryProtocol {

    func queryShops(boundingBox: String) -> Single<[ShopRepoModel], Error>

}

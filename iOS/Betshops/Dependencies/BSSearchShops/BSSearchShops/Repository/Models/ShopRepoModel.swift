struct ShopRepoModel {

    let id: Int
    let name: String
    let location: LocationRepoModel
    let county: String
    let cityId: Int
    let city: String
    let address: String

}

extension ShopRepoModel {

    init(from model: ShopDataModel) {
        id = model.id
        name = model.name
        location = LocationRepoModel(from: model.location)
        county = model.county
        cityId = model.cityId
        city = model.city
        address = model.address
    }

}

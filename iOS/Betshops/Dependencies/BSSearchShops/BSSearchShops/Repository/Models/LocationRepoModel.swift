struct LocationRepoModel {

    let latitude: Double
    let longitude: Double

}

extension LocationRepoModel {

    init(from model: LocationDataModel) {
        latitude = model.latitude
        longitude = model.longitude
    }

}

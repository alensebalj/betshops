import BSCore

class SearchShopsRepository: SearchShopsRepositoryProtocol {

    private let dataSource: SearchShopsDataSourceProtocol

    init(dataSource: SearchShopsDataSourceProtocol) {
        self.dataSource = dataSource
    }

    func queryShops(boundingBox: String) -> Single<[ShopRepoModel], Error> {
        dataSource
            .queryShops(boundingBox: boundingBox)
            .compactMap { [weak self] in
                self?.mapToModels($0)
            }
            .asSingle()
    }

    private func mapToModels(_ dataModels: [ShopDataModel]) -> [ShopRepoModel] {
        dataModels
            .map { ShopRepoModel(from: $0) }
    }

}

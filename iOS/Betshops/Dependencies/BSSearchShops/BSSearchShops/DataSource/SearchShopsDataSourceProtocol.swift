import BSCore

protocol SearchShopsDataSourceProtocol {

    func queryShops(boundingBox: String) -> Single<[ShopDataModel], Error>

}

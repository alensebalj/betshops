struct LocationDataModel {

    let latitude: Double
    let longitude: Double

}

extension LocationDataModel {

    init(from model: LocationResponse) {
        latitude = model.lat
        longitude = model.lng
    }

}

struct ShopDataModel {

    let id: Int
    let name: String
    let location: LocationDataModel
    let county: String
    let cityId: Int
    let city: String
    let address: String

}

extension ShopDataModel {

    init(from model: ShopResponse) {
        id = model.id
        name = model.name
        location = LocationDataModel(from: model.location)
        county = model.county
        cityId = model.cityId
        city = model.city
        address = model.address
    }

}

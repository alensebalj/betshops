import BSCore

class SearchShopsDataSource: SearchShopsDataSourceProtocol {

    private let searchShopsClient: SearchShopsClientProtocol

    init(searchShopsClient: SearchShopsClientProtocol) {
        self.searchShopsClient = searchShopsClient
    }

    func queryShops(boundingBox: String) -> Single<[ShopDataModel], Error> {
        let shopRequest = ShopRequest(boundingBox: boundingBox)

        return searchShopsClient
            .queryShops(request: shopRequest)
            .compactMap { [weak self] in
                self?.mapToDataModels($0.betshops)
            }
            .subscribeOnBackground()
    }

    private func mapToDataModels(_ responseModels: [ShopResponse]) -> [ShopDataModel] {
        responseModels
            .map { ShopDataModel(from: $0) }
    }

}

import BSCore

enum LocalizableStrings: String, Localizable {

    case betShops = "searchShops.betShops"

    case cityCounty = "shopDetails.cityCounty"
    case route = "shopDetails.route"
    case openUntil = "shopDetails.openUntil"
    case opensTomorrow = "shopDetails.opensTomorrow"

    var tableName: String {
        "SearchShopsLocalizable"
    }

    var bundle: Bundle {
        .local
    }

}

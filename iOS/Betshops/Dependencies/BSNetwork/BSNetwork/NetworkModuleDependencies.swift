import Resolver

public enum NetworkModuleDependencies: String {

    case apiBaseUrl

    public var name: Resolver.Name {
        Resolver.Name(rawValue)
    }

}

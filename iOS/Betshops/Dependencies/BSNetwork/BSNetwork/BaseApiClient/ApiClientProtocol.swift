import Combine
import BSCore

public protocol ApiClientProtocol {

    func get<ResultType: Decodable>(path: String, queryParameters: [String: String]?) -> Single<ResultType, Error>

    func post<ParamsType: Encodable>(path: String, body: ParamsType) -> Completable<Error>

    func post<ParamsType: Encodable, ResultType: Decodable>(
        path: String,
        body: ParamsType
    ) -> Single<ResultType, Error>

    func put<ParamsType: Encodable>(path: String, body: ParamsType) -> Completable<Error>

    func patch<ParamsType: Encodable, ResultType: Decodable>(
        path: String,
        body: ParamsType
    ) -> Single<ResultType, Error>

    func delete(path: String, parameters: [String: String]?) -> Completable<Error>

    func buildUrl(path: String, method: HTTPMethod, parameters: [String: String]?) -> URL?

    func buildRequest(path: String, method: HTTPMethod, parameters: [String: String]?) -> URLRequest?

}

// ApiClientProtocol extension that adds support for default parameters.
public extension ApiClientProtocol {

    func get<ResultType: Decodable>(
        path: String,
        queryParameters: [String: String]? = nil
    ) -> Single<ResultType, Error> {
        get(path: path, queryParameters: queryParameters)
    }

    func delete(path: String, parameters: [String: String]? = nil) -> Completable<Error> {
        delete(path: path, parameters: parameters)
    }

}

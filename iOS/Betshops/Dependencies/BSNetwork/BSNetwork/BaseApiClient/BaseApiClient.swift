import BSCore
import Combine

class BaseApiClient: ApiClientProtocol {

    private let baseUrl: String
    private let simpleApiClient: SimpleApiClient

    init(baseUrl: String, urlSession: URLSession) {
        self.baseUrl = baseUrl
        self.simpleApiClient = SimpleApiClient(urlSession: urlSession)
    }

    func get<ResultType: Decodable>(
        path: String,
        queryParameters: [String: String]? = nil
    ) -> Single<ResultType, Error> {
        simpleApiClient.executeAndReturn(url: url(with: path), method: .get, parameters: queryParameters)
    }

    func post<ParamsType: Encodable>(path: String, body: ParamsType) -> Completable<Error> {
        simpleApiClient.execute(url: url(with: path), method: .post, parameters: body)
    }

    func post<ParamsType: Encodable, ResultType: Decodable>(
        path: String,
        body: ParamsType
    ) -> Single<ResultType, Error> {
        simpleApiClient.executeAndReturn(url: url(with: path), method: .post, parameters: body)
    }

    func put<ParamsType: Encodable>(path: String, body: ParamsType) -> Completable<Error> {
        simpleApiClient.execute(url: url(with: path), method: .put, parameters: body)
    }

    func patch<ParamsType: Encodable, ResultType: Decodable>(
        path: String,
        body: ParamsType
    ) -> Single<ResultType, Error> {
        simpleApiClient.executeAndReturn(url: url(with: path), method: .patch, parameters: body)
    }

    func delete(path: String, parameters: [String: String]? = nil) -> Completable<Error> {
        simpleApiClient.execute(url: url(with: path), method: .delete, parameters: parameters)
    }

    func buildUrl(path: String, method: HTTPMethod, parameters: [String: String]?) -> URL? {
        simpleApiClient.buildUrl(url: url(with: path), method: method, parameters: parameters)
    }

    func buildRequest(path: String, method: HTTPMethod, parameters: [String: String]?) -> URLRequest? {
        simpleApiClient.buildRequest(url: url(with: path), method: method, parameters: parameters)
    }

    private func url(with path: String) -> String {
        "\(baseUrl)\(path)"
    }

}

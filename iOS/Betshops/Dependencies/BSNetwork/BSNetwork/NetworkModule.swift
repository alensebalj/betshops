import BSCore
import Resolver

public class NetworkModule: AppModuleProtocol {

    public init() {}

    public func registerDependencies(in container: Resolver) {
        registerClients(in: container)
    }

    private func registerClients(in container: Resolver) {
        container.register(ApiClientProtocol.self, name: NetworkModuleDependencies.apiBaseUrl.name) {
            let baseUrl = container.resolve(String.self, name: NetworkModuleDependencies.apiBaseUrl.name)
            return BaseApiClient(baseUrl: baseUrl, urlSession: URLSession.shared)
        }
        .scope(.application)
    }

}

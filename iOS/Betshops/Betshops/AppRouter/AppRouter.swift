import Resolver
import UIKit

class AppRouter {

    private weak var navigationController: UINavigationController?

    let container: Resolver

    var rootNavigationController: UINavigationController? {
        navigationController
    }

    init(navigationController: UINavigationController, container: Resolver) {
        self.navigationController = navigationController
        self.container = container
    }

    func start(in window: UIWindow) {
        window.rootViewController = navigationController
        window.makeKeyAndVisible()

        configureNavigationBar()
        startApp()
    }

    private func configureNavigationBar() {
        let navBarAppearance = UINavigationBarAppearance()
        navBarAppearance.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        navBarAppearance.largeTitleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        navBarAppearance.backgroundColor = .green

        navigationController?.navigationBar.isTranslucent = false

        navigationController?.navigationBar.standardAppearance = navBarAppearance
        navigationController?.navigationBar.scrollEdgeAppearance = navBarAppearance
    }

    private func startApp() {
        goToSearchShopsViewController()
    }

}

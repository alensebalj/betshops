import BSCoreUI
import BSSearchShops

extension AppRouter: SearchShopsRouterProtocol {

    func goToSearchShopsViewController() {
        let searchShopsViewController: SearchShopsViewController = container.resolve()
        setViewController(searchShopsViewController)
    }

    func showDetails(with viewModel: ShopViewModel) {
        let modalPopupViewController: ModalPopupViewController = container.resolve(args: viewModel)
        modalPopupViewController.modalPresentationStyle = .overCurrentContext
        presentViewController(modalPopupViewController)
    }

}

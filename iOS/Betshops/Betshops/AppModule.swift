import BSCore
import BSLocation
import BSNetwork
import BSSearchShops
import Resolver
import UIKit

class AppModule: AppModuleProtocol {

    /**
     Collection of modules that will be initialized during app bootstrap cycle
    */
    private let modules: [AppModuleProtocol] = [
        SearchShopsModule(),
        LocationModule(),
        NetworkModule()
    ]

    private lazy var container: Resolver = {
        let container = Resolver()
        registerDependencies(in: container)
        return container
    }()

    lazy var appRouter: AppRouter = {
        container.resolve()
    }()

    /**
    Registers app level dependencies to container, and afterwards initializes
    dependencies of each module.

    - Parameter container: Container in  which dependencies will be registered.
    */
    func registerDependencies(in container: Resolver) {
        registerAppDependencies(in: container)

        modules.forEach { $0.registerDependencies(in: container) }
    }

    /**
    Registers app level dependencies

    - Parameter container: Container in  which dependencies will be registered
    */
    private func registerAppDependencies(in container: Resolver) {
        container
            .register(name: AppDependencies.navigationControllerRoot.name) { UINavigationController() }
            .scope(.application)

        container
            .register(String.self, name: NetworkModuleDependencies.apiBaseUrl.name) {
                (Bundle.main.infoDictionary?["ApiUrl"] as? String) ?? ""
            }
            .scope(.application)

        container
            .register { _ -> AppRouter in
                let navigationController = self.container.resolve(
                    UINavigationController.self,
                    name: AppDependencies.navigationControllerRoot.name)

                return AppRouter(
                    navigationController: navigationController,
                    container: container)
            }
            .scope(.application)

        registerRouters(in: container)
    }

    private func registerRouters(in container: Resolver) {
        container
            .register(SearchShopsRouterProtocol.self) {
                let appRouter: AppRouter = self.container.resolve()
                return appRouter
            }
            .scope(.application)
    }

}

extension AppModule {

    /**
     Collection of named dependencies used on app level, e.g. app level navigation controller or services.
    */
    private enum AppDependencies: String {

        case navigationControllerRoot = "NavigationController.Root"

        var name: Resolver.Name {
            Resolver.Name(rawValue)
        }

    }

}
